package main

import "bitbucket.org/ahristov/pluralsight-creating-web-applications-go/p03/fileserv"

func main() {
	fileserv.RunWithAuto()
}
