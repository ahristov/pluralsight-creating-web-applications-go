package fileserv

import (
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

// RunWithSwitch runs example with manual switch of content type based on file extention.
func RunWithSwitch() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		f, err := os.Open("public" + r.URL.Path)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Println(err)
		}
		defer f.Close()
		var contentType string
		switch {
		case strings.Contains(r.URL.Path, ".css"):
			contentType = "text/css"
		case strings.Contains(r.URL.Path, ".html"):
			contentType = "text/html"
		case strings.HasSuffix(r.URL.Path, ".png"):
			contentType = "image/png"
		case strings.HasSuffix(r.URL.Path, ".ico"):
			contentType = "image/x-icon"
		default:
			contentType = "text/plain"
		}
		w.Header().Add("Content-Type", contentType)
		io.Copy(w, f)
	})

	http.ListenAndServe(":8000", nil)
}
