package fileserv

import (
	"net/http"
)

// RunWithAuto runs example with automatically resolving content type.
func RunWithAuto() {
	http.ListenAndServe(":8000", http.FileServer(http.Dir("public")))
}
