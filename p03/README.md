Special handlers
===

*NotFoundHandler*: Use for example when accessing non-authorized area.

    func NotFoundHandler() Handler

*RedirectHandler*: where to redirect and what http status code (temporary, permanent, etc.).

    func RedirectHandler(url string, code int) Handler

*StripPrefix* handler: Handle url that has a prefix, strip out the prefix and send to another handler.

    func StripPrefix(prefix string, h handler) Handler

*TimeoutHandler*: Actually serves as a decorator.

    func TimeoutHandler(h Handler, dt time.Duration, msg string) Handler

- what handler is it decorating

- amount of time that handler is allowed to process

- string to respond if it takes too long

*FileServer*: FileSystem - takes name of file and receives file object.

    func FileServer(root FileSystem) Handler

    type FileSystem interface {
        Open(name string) (File, error)
    }

Implementation of FileSystem is `Dir`:

    type Dir string
    func (d Dir) Open(name string) (File, error)

Note: `Dir` is an alias to string

