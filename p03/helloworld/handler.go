package helloworld

import (
	"fmt"
	"net/http"
)

type myHandler struct {
	greeting string
}

/***
	Implement the Handler interface
	===

	func HandleFunc(pattern string, handler HandlerFunc)

	type HandlerFunc (w ResponseWriter, t Request)
	func (f HandlerFunc) ServeHTTP(w ResponseWriter, r *Request)
***/

func (mh myHandler) ServeHTTP(w http.ResponseWriter, t *http.Request) {
	w.Write([]byte(fmt.Sprintf("%v go world", mh.greeting)))
}

// RunWithHandler runs example with handler.
func RunWithHandler() {
	http.Handle("/", &myHandler{greeting: "Hello"})
	http.ListenAndServe(":8000", nil)
}
