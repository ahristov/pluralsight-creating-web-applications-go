package helloworld

import (
	"net/http"
)

// RunWithFunc runs example with anonymous function handler.
func RunWithFunc() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello World!"))
	})
	http.ListenAndServe(":8000", nil)
}
